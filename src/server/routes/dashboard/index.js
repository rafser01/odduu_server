var router = require('express').Router();
var clientList = require('./removeClient').removeClient;
var tokenGenerate = require('./tokenGenerate').tokenGenerate;
var removeClient = require('./removeClient').removeClient;
var addLicense = require('./addLicense').addLicense;

router.use('/removeClient', removeClient );
router.use('/tokenGenerate', tokenGenerate);
router.use('/addLicense', addLicense);
exports.dashboard = router;
