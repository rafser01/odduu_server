var mongoose = require('../init').mongoose;
var Schema = mongoose.Schema;

var clientSchema = new Schema({
  orgName: { type: String, required : true, unique : true, dropDups: true },
  contactName: {type: String, required: true},
  contactCell: {type: String, default: '01700-000000'},
  contactEmail: {type: String, default: null},
  licenseQty: {type: [{
    expDate: Date,
    startedDate: Date,
    qty: Number
  }], required: true}
})

exports.Client = mongoose.model('Client', clientSchema);
