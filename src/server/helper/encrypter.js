import {SECRET_KEY_SOCKET} from '../../App/stores/constants';
var encrypter = require('object-encrypter');

const engine = encrypter(SECRET_KEY_SOCKET);
export const encrypterFun = (obj) => {
  return engine.encrypt(obj);
}

export const decrypterFun = (hex) => {
  try {
    return engine.decrypt(hex);
  } catch (e) {
    console.error('Error in parsing hax -> encrypter File -> ',e);
    return false;
  }
}
